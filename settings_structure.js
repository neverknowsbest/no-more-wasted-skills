module.exports = [
    {
        "key": "enabled",
        "name": "Enabled",
        "type": "bool"
    },
    {
        "key": "cablesurf",
        "name": "(Reaper) Allows Cable Step to work on surfaces",
        "type": "bool"
    },
    {
        "key": "bindingscythes",
        "name": "(Reaper) Lets Binding Scythes work with targets only",
        "type": "bool"
    }
];
