[![Toolbox](https://img.shields.io/badge/Tera--Toolbox-latest-blueviolet)](https://github.com/tera-toolbox) ![](https://img.shields.io/github/license/SaltyMonkey/backstabs-manager)

# No More Wasted Skills

This module prevents the user from casting certain skills without a target.

# Supported Skills

- Most classes
  - Backstab
- Brawler
  - Meat Grinder
- Lancer
  - Chained Leash
  - Leash
  - Master Leash
- Reaper
  - Binding Scythes
  - Cable Step

# Compatibility

Should work on patches 92 and 115. 100 not supported at the moment.

# Installation

You can either:
  - Click the download button next to "Clone", then select any format (usually .zip) and extract it inside `<TeraToolbox>/mods`
  - Create a folder called `no-more-wasted-skills` inside `<TeraToolbox/mods>`, then place [`modules.json`](https://gitlab.com/neverknowsbest/no-more-wasted-skills/-/raw/master/module.json) inside and start Toolbox
  - If you have git installed, run `git clone https://gitlab.com/neverknowsbest/no-more-wasted-skills` inside `<TeraToolbox>/mods`


# Commands

You can either use `bm` or `ws` for controlling the mod within Toolbox channel (`/8` or `/toolbox`).

`bm/ws` - Toggle the module

`bm/ws bindscy` - Makes the skill "Binding Scythes" require a target. **Reaper only**

`bm/ws cs` - Toggle cable step usage on surfaces. **Reaper only**

`bm/ws debug` - Enables debug mode. Does not persist between logins.

`bm/ws help` - Prints usage in toolbox chat.

`bm/ws rebuild` - Rebuild skill data. Also called on user login.

`bm/ws reload` - Reload the module. You can also use `toolbox reload no-more-wasted-skills`

# Known Issues

- Toggling the mod with `bm` or `ws` does not affect backstab skills.

# TODO

- Fix module toggle not affecting skills of catchback type (backstab/meat grinder).
- Add more skills?
- Disable catchback skill handling if user has NGSP installed
  - Alternatively, just set `preventEmptyBackstabsCasts` to false. ¯\\_(ツ)_/¯

# Credits

- **Saltymonkey:** Original author (backstabs-manager)