// reaper
const BINDING_SCYTHES_ID = 230100; // C_START_TARGETED_SKILL
const CABLE_STEP_ID = 200100;      // C_START_TARGETED_SKILL
// lancer
const CHAINED_LEASH_ID = 240101; // C_START_INSTANCE_SKILL
// warrior
const BINDING_SWORD_ID = 340100; // C_START_SKILL too? don't remember - add later

function noMoreWastedSkills(mod) {
    let skillData = new Set();
        let debug = false;
        let skillType = [
            'catchback',     // backstab (most classes), meat grinder (brawler)
            'capture',       // leash, master leash (lancer)
            'inversecapture' // cable step, binding scythes (reaper)
        ];

        mod.command.add(['bm', 'ws'], {
              '$none': () => {
                    mod.settings.enabled = !mod.settings.enabled;
                    mod.command.message((mod.settings.enabled ? "Enabled" : "Disabled"))
              },
              '$default': () => {
                    mod.command.message('Invalid command! Run <font color="#4e5ab7">bm/ws help</font> for more info.')
              },
              'cs': () => {
                    mod.settings.cablesurf = !mod.settings.cablesurf;
                    mod.command.message("Cable Step now " + (mod.settings.cablesurf ? "includes" : "ignores") + " surfaces.")
              },
              'bindscy': () => {
                    mod.settings.bindingscythes = !mod.settings.bindingscythes;
                    mod.command.message("Binding Scythes support is now " + (mod.settings.bindingscythes ? "en" : "dis") + "abled.")
              },
              'rebuild': () => {
                    mod.command.message("Rebuilding skill data...")
                    if(debug) mod.command.message("(DEBUG) Class: " + mod.game.me.class);
                    buildData();
                    mod.command.message("Done!");
              },
              'debug': () => {
                    debug = !debug;
                    mod.command.message("Debug is now " + (debug ? "en" : "dis") + "abled.");
              },
              'status': () => {
                    mod.command.message(`Enabled: ${mod.settings.enabled}`);
                    mod.command.message(`Debug: ${debug}`);
                    mod.command.message(`Cable Step surfing: ${mod.settings.cablesurf}`);
                    mod.command.message(`Binding Scythes: ${mod.settings.bindingscythes}`);
              },
              'reload': () => {
                    try {
                        mod.manager.reload(mod.info.name);
                        mod.command.message("<font color='#2dc55e'>Reloaded!</font>");
                    } catch (e) {
                        mod.command.message("<font color='#f81118'>Reload failed.</font> Please check Toolbox log for more info.");
                    }
              },
              'help': () => {
                    mod.command.message("Usage: bm/ws command");
                    mod.command.message("No argument: Turns the module on/off.");
                    mod.command.message("cs: (Reaper) Lets you use Cable Step on surfaces as well - off by default");
                    mod.command.message("bindscy: (Reaper) Makes Binding Scythes require a target.");
                    mod.command.message("rebuild: Rebuild skill data. This is also called upon character login.");
                    mod.command.message("debug: Enables debug mode - does not persist between logins!");
                    mod.command.message("help: Show this message.");
                    mod.command.message("reload: Reloads the module.");
              }
        });

        function buildData() {
                skillData.clear();
                mod.queryData("/SkillData/Skill@templateId=?/", [mod.game.me.templateId], true, false, ["id", "type"]).then(res => {
                        res.forEach(skill => {
                            if (skillType.includes(skill.attributes.type.toLowerCase())) {
                                skillData.add(skill.attributes.id);
                            };
                        });
                });

                // I need a better way of specifying other skills... for now we stick with this /shrug
                switch(mod.game.me.class) {
                        case 'lancer':
                                skillData.add(CHAINED_LEASH_ID); // Chained Leash a.k.a Giga
                                break;
                        case 'warrior':
                                skillData.add(BINDING_SWORD_ID); // Binding Swords, AoE pull
                                break;
                };
        };

        function blockSkill(skillID) {
                if (debug) { mod.command.message(`(DEBUG) Blocked skill ${skillID}.`)};
                mod.send("S_CANNOT_START_SKILL", 4, { skill: skillID });
        };

        // adds skill data on character login
        mod.game.on("enter_game", () => {
                buildData();
        });

        // prevents stuff from casting (or not)
        mod.hook("C_START_TARGETED_SKILL", 7, { "order": -105 }, event => {
                if (!mod.settings.enabled) return;

                /* ---------- REAPER EXCLUSIVE SHIT ---------- */
                // lets you cast cable step in the ground or somewhere else than a mob/player for mobility
                if (mod.settings.cablesurf && event.dest.x && skillData.has(CABLE_STEP_ID)) return;

                // binding scythes works as an iframe even if you miss it, should be good for pvp
                if (!mod.settings.bindingscythes && skillData.has(BINDING_SCYTHES_ID)) return;
                /* ---------- END OF REAPER EXCLUSIVE SHIT ---------- */

                // prevents backtstab, leash or other stuff from casting without a target
                if (!event.targets[0].gameId && skillData.has(event.skill.id)) {
                        blockSkill(event.skill.id);
                        return false;
            };
        });

        // same as above, but for now, only chained leash uses this
        mod.hook("C_START_INSTANCE_SKILL", mod.majorPatchVersion >= 114 ? 8 : 7, { "order": -105 }, event => {
                if (!mod.settings.enabled) return;

                // only chained leash uses this for now, like mentioned above
                if (!event.targets[0] && skillData.has(event.skill.id)) {
                        blockSkill(event.skill.id);
                        return false;
                };
        });

        // Reload support
        this.destructor = () => {
                mod.command.remove(['bm', 'ws']);
        };

        this.saveState = () => {
                mod.log('user called reload, saving mod state');
                return {
                    debug: debug
                };
        };
        this.loadState = (state) => {
                mod.log('loading state')
                debug = state.debug;

                mod.log('rebuilding skill data...');
                buildData();
                mod.log('done!');
        };
}

exports.NetworkMod = noMoreWastedSkills;
